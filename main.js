
const images = document.querySelectorAll('.image-to-show');
const stopButton = document.querySelector('.stop-button');
const nextButton = document.querySelector('.next-button');
let currentIndex = 0;
let interval;
for(let i = 0; i < images.length;i++){
    if(images[currentIndex] === images[i]){
        images[currentIndex].style.display='block';
    }else{
        images[i].style.display='none'
    }
}
function nextImage() {
  images[currentIndex].style.display = 'none';
  currentIndex = currentIndex === images.length - 1 ? 0 : ++currentIndex;
  images[currentIndex].style.display = 'block';
}
 function startShow(){
    interval =  setInterval(nextImage,3000);
 }
 startShow();

 nextButton.addEventListener('click',startShow);

 function stopShow(){
    clearInterval(interval);
 }

 stopButton.addEventListener("click",stopShow);

 
 